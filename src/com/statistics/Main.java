package com.statistics;

import com.statistics.components.ErrorStatistics;
import com.statistics.enums.TimeStepEnum;

public class Main {

    public static void main(String[] args) {
        System.out.println("Start");
        ErrorStatistics.run(System.getProperty("user.dir") + "/logs", TimeStepEnum.HOUR);
        System.out.println("End");
    }
}
