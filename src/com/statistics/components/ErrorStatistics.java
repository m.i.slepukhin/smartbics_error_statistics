package com.statistics.components;

import com.statistics.enums.TimeStepEnum;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 *  Класс, который запускает сбор статистики
 *
 */
public class ErrorStatistics {

    private static final String STATISTICS_FILE_NAME = "error_statistics.txt";     //  Результирующий файл со статистикой
    private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm");

    private static Map<LocalDateTime, Integer> statistics = new HashMap<>();   //  Map, в которой собирается статистика
    private static String path;
    private static TimeStepEnum step;

    public static void run(String pathLogs, TimeStepEnum timeStep) {
        path = pathLogs;
        step = timeStep;
        try {
            calculateStatistics();
        } catch (NoSuchFileException e) {
            System.err.println("Ошибка указания пути: " + e);
        } catch (IOException e) {
            System.err.println("Ошибка ввода-вывода:" + e);
        }
    }

    private static void calculateStatistics() throws IOException {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(path), "*.log")) {
            for(Path entry: directoryStream) statisticsAggregation(new LogFile(entry).calculateStatistics(step));
            writeStatisticsInFile();
        }
    }

    private  static void statisticsAggregation(Map<LocalDateTime, Integer> fileStatistics) {
        fileStatistics.forEach((k, v) -> statistics.put(k, statistics.containsKey(k) ? statistics.get(k) + v : 1));
    }

    private static void writeStatisticsInFile() throws IOException {
        List<String> lines = sortedMap(statistics)
                .entrySet()
                .stream()
                .map(e -> getStatisticsDateTime(e.getKey()) + " Количество ошибок: " + e.getValue())
                .collect(Collectors.toList());

        Path file = Paths.get(path + "/" + STATISTICS_FILE_NAME);
        Files.write(file, lines, StandardCharsets.UTF_8);
        System.out.println("Статистика собрана! Файл: " + file.toString());
    }

    private static Map<LocalDateTime, Integer> sortedMap(Map<LocalDateTime, Integer> map) {
        return map.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1, o2) -> o1, TreeMap::new));
    }

    private static String getStatisticsDateTime(LocalDateTime date) {
        return date.format(dateFormat) + ", " + date.format(timeFormat) + "-" + plusStep(date).format(timeFormat);
    }

    private static LocalDateTime plusStep(LocalDateTime date) {
        return step == TimeStepEnum.MINUTE ? date.plusMinutes(1L) : date.plusHours(1L);
    }
}
