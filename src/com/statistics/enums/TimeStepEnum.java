package com.statistics.enums;

public enum TimeStepEnum {
    MINUTE,
    HOUR;
}
